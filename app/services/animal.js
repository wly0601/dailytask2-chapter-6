const {animal} = require("../models")

module.exports = {
	getAll(req,res){
		return animal.findAll()
	},
	animalCreate(input){
		return animal.create(input)
	},
	update(currentData, updatedData){
		return currentData.update(updatedData)
	},
	findKey(input){
		return animal.findByPk(input)
	},
	destroy(selectedAnimal){
		return selectedAnimal.destroy()
	}
}
