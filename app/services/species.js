const { speciesName } = require("../models");

module.exports = {
    getAll(req,res) {
        return speciesName.findAll()
    },
    createSpecies(input){
        return speciesName.create(input)
    },

    updateSpecies(curentData, updatedData){
        return curentData.update(updatedData)
    },
    findKey(input){
        return speciesName.findByPk(input)
    },
    deleteSpecies(selectedSpecies){
        return selectedSpecies.destroy()
    }
}
