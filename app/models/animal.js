'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class animal extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      animal.belongsTo(models.speciesName, {
        foreignKey: 'speciesId'
      });
    }
  }
  animal.init({
    speciesId: DataTypes.INTEGER,
    species: DataTypes.STRING,
    genus: DataTypes.STRING,
    family: DataTypes.STRING,
    order: DataTypes.STRING,
    classAnimal: DataTypes.STRING,
    phylum: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'animal',
  });
  return animal;
};