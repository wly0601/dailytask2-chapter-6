'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class speciesName extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      speciesName.hasMany(models.animal, {
        foreignKey: 'speciesId'
      });
    }
  }
  speciesName.init({
    name: DataTypes.STRING,
    color: DataTypes.STRING,
    ageInMonts: DataTypes.INTEGER,
    lengthInCentimeters: DataTypes.INTEGER,
    massInKilograms: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'speciesName',
  });
  return speciesName;
};