
const { animal,speciesName } = require("../../../models");
const animalService = require("../../../services/animal")

module.exports = {
  list(req, res) {
    const showAnimals = animalService.getAll({
      include:{
        model:speciesName,
      }
    })
    .then((animals) => {
        res.status(200).json({
          status: "OK",
          data: {
            animals,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      })
  },

  create(req, res) {
    const { speciesId, species, genus, family, order, classAnimal, phylum } = req.body

    animalService.animalCreate(req.body)
      .then((animal) => {
        res.status(201).json({
          status: "OK",
          data: animal,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    const animal = req.animal
    const {speciesId, species, genus, family, order, classAnimal, phylum} = req.body

    const update = animalService.update(animal, req.body)
      .then(() => {
        res.status(200).json({
          status: "OK",
          data: animal,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const animal = req.animal;

    res.status(200).json({
      status: "OK",
      data: animal,
    });
  },

  destroy(req, res) {
    const deleteAnimal = animalService.destroy(req.animal)
      .then(() => {
        res.status(204).end()
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setAnimal(req, res, next) {
    const findPk = animalService.findKey(req.params.id)
      .then((animal) => {
        if (!animal) {
          res.status(404).json({
            status: "FAIL",
            message: "Animal not found!",
          });

          return;
        }

        req.animal = animal;
        next()
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Animal not found!",
        });
      });
  },
};
