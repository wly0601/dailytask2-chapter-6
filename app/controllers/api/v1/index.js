const animal = require("./animal");
const species = require("./species");

module.exports = {
  animal,
  species
};
