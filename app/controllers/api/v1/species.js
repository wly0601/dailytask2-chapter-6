const { animal, speciesName } = require("../../../models");
const speciesService = require("../../../services/species");

module.exports = {
  list(req, res) {
      const showSpecies = speciesService.getAll({
        include :{
          model : animal,
          attributes: [
            'speciesId', 'species', 'genus', 'family', 'order', 'classAnimal', 'phylum'
          ]
        }
      })
      .then((species) => {
        res.status(200).json({
          status: "OK",
          data: {
            species,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
      

  create(req, res) {
    const {name, color, ageInMonts, lengthInCentimeters, massInKilograms} = req.body;
    const createSpecies = speciesService.createSpecies(req.body)
      .then((species) => {
        res.status(201).json({
          status: "OK",
          data: species,
        });
      })
      .catch((err) => {
        res.status(401).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    const species = req.species;
    const {name, color, ageInMonts, lengthInCentimeters, massInKilograms} = req.body

    const updateSpecies = speciesService.updateSpecies( species, req.body )
      .then(() => {
        res.status(200).json({
          status: "OK",
          data: species,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const species = req.species;

    res.status(200).json({
      status: "OK",
      data: species,
    });
  },

  destroy(req, res) {
    const deleteSpecies = speciesService.deleteSpecies(req.species)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setSpecies(req, res, next) {
    const findSpeciesByKey = speciesService.findKey(req.params.id)
      .then((species) => {
        if (!species) {
          res.status(404).json({
            status: "FAIL",
            message: "Species not found!",
          });

          return;
        }

        req.species = species;
        next()
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Species not found!",
        });
      });
  },
};
