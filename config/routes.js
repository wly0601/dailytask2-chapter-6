const express = require("express");
const controllers = require("../app/controllers");

const appRouter = express.Router();
const apiRouter = express.Router();

/** Mount GET / handler */
appRouter.get("/", controllers.main.index);

/**
 * Animal API
 */
apiRouter.get("/api/v1/animals", controllers.api.v1.animal.list);
apiRouter.post("/api/v1/animals", controllers.api.v1.animal.create);
apiRouter.put(
  "/api/v1/animals/:id",
  controllers.api.v1.animal.setAnimal,
  controllers.api.v1.animal.update
);
apiRouter.get(
  "/api/v1/animals/:id",
  controllers.api.v1.animal.setAnimal,
  controllers.api.v1.animal.show
);
apiRouter.delete(
  "/api/v1/animals/:id",
  controllers.api.v1.animal.setAnimal,
  controllers.api.v1.animal.destroy
);

/**
 * Species API
 */

 apiRouter.get("/api/v1/species", controllers.api.v1.species.list);
 apiRouter.post("/api/v1/species", controllers.api.v1.species.create);
 apiRouter.put(
   "/api/v1/species/:id",
   controllers.api.v1.species.setSpecies,
   controllers.api.v1.species.update
 );
 apiRouter.get(
  "/api/v1/species/:id",
  controllers.api.v1.species.setSpecies,
  controllers.api.v1.species.show
);
apiRouter.delete(
  "/api/v1/species/:id",
  controllers.api.v1.species.setSpecies,
  controllers.api.v1.species.destroy
);


/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
appRouter.get("/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

appRouter.use(apiRouter);

/** Mount Not Found Handler */
appRouter.use(controllers.main.onLost);

/** Mount Exception Handler */
appRouter.use(controllers.main.onError);

module.exports = appRouter;
