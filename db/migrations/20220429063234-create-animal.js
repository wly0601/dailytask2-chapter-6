'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('animals', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      speciesId: {
        type: Sequelize.INTEGER
      },
      species: {
        type: Sequelize.STRING
      },
      genus: {
        type: Sequelize.STRING
      },
      family: {
        type: Sequelize.STRING
      },
      order: {
        type: Sequelize.STRING
      },
      classAnimal: {
        type: Sequelize.STRING
      },
      phylum: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('animals');
  }
};