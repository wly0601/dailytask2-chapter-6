'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('speciesNames', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      color: {
        type: Sequelize.STRING
      },
      ageInMonts: {
        type: Sequelize.INTEGER
      },
      lengthInCentimeters: {
        type: Sequelize.INTEGER
      },
      massInKilograms: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('speciesNames');
  }
};